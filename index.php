<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Clippings</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <style>
    form {
      background: #ddd;
      border: 1px solid;
    }

    input,
    select {
      width: 100px;
    }

    label {
      width: 100px;
      text-align: left;
    }

    table,
    th,
    td {
      border: 1px solid black;
      width: 300px;
    }

    table {
      width: 100%;
    }

    table>tr:first-child>* {
      background: #000;
      color: #fff;
    }
  </style>
</head>

<body>
  <div class="mt-5">
    <div class="container">
      <div class="row">
        <div class="col-md-6 m-auto">
          <div class="text-center">
            <form action="getSum.php" method="post" id="form" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-6 m-auto">
                  <div class="m-2">
                    <label for="csv_file">Select file:</label>
                    <input type="file" name="csv_file" accept=".csv" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 m-auto">
                  <div class="m-2">
                    <label for="customers">Customers:</label>
                    <select name="customers" id="">
                      <option value="">ALL</option>
                      <option value="Vendor 1">Vendor 1</option>
                      <option value="Vendor 2">Vendor 2</option>
                      <option value="Vendor 3">Vendor 3</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 m-auto">
                  <div class="m-2">
                    <label for="currency">Currency:</label>
                    <select name="currency" id="currency">
                      <option value="USD">USD</option>
                      <option value="EUR">EUR</option>
                      <option value="GBP">GBP</option>
                      <option value="BGN">BGN</option>

                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 m-auto">
                  <div class="m-2">
                    <input type="text" hidden name="getSum">
                    <input type="submit" name="submit" value="submit">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 m-auto">
          <div class="text-center">
            <table id="table">


            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script>
    $('#form').submit(getSum)

    function getSum(e) {
      e.preventDefault();

      clearElement('#table')

      url = 'getSum.php'
      const body = new FormData(this)
      const method = 'post'

      const data = request(url, body, method)

      data.then(response => (response.json())).then(body => {
        if (body.error) {
          console.log(body)
          return alert(body.error)
        }
        
        const currency = $('#currency').val()

        $('#table').append(`<tr>
                              <th>Customer</th>
                              <th>Total Price (${currency})</th>
                           </tr>`)
        for (const [key, value] of Object.entries(body)) {
          $('#table').append(`<tr>
                                <th>${key}</th>
                                <th>${value.toFixed(4)}</th>
                             </tr>`)
        }
      }).catch(err => {
        
        alert('ERROR')
      });;

    }

    function request(url, body, method) {
      return fetch(url, {
        body,
        method
      })
    }

    function clearElement(el) {
      $(el).empty()
    }
  </script>
</body>

</html>