<?php

spl_autoload_register(function ($class_name) {

  $class_name = str_replace('\\', DIRECTORY_SEPARATOR, $class_name);

  $class_file = "classes/$class_name.php";

  if (!file_exists($class_file)) {
    throw new Exception("Class $class_file not found");
  } else {
    require_once  $class_file;
  }
});
