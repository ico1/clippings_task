<?php


namespace Calculations;

class CalculatePrice
{
  private $data;
  private $currency;
  private $currencies;
  private $customer;



  public function setData($data)
  {
    $this->data = $data;
  }
  public function setCurrency($currency)
  {
    if (!is_string($currency)) {
      throw new \Exception('Currency must be string');
    }
    if (strlen($currency) != 3) {
      throw new \Exception('Currency length is invalid');
    } else {
      $this->currency = $currency;
    }
  }
  public function setCustomer($customer)
  {
    $this->customer = $customer;
  }
  public function setCurrencies($currencies)
  {
    $this->currencies = $currencies;
  }

  public function getCurrency()
  {
    return $this->currency;
  }
  public function getCustomer()
  {
    return $this->customer;
  }

  public function getTotal()
  {

    if (empty($this->data)) {
      throw new \Exception('Data is required');
    }
    if (empty($this->currency)) {
      throw new \Exception('Currency is required');
    }
    if (empty($this->currencies[$this->currency])) {
      throw new \Exception('Currency not supported');
    }
    $sorted_data = [];
    $total_price = 0;

    /* CONVERT ALL TO EUR THEN TO SELECTED CURRENCY */

    foreach ($this->data as $line) {
      $selected_currency_rate = $this->currencies[$this->currency];

      $customer =  $line['Customer'];
      $currency =  $line['Currency'];

      $price = $line['Total'];
      @$temp_price = $sorted_data[$customer];
      $price = $price * $this->currencies[$currency];  // CONVERT TO EUR

      $type = $line['Type'];
      if ($type == 2) {
        $price = -$price;
      }


      if (empty($temp_price)) {
        $sorted_data[$customer] = $price;
      } else {

        $total_price = ($temp_price) + ($price);
        $sorted_data[$customer] = $total_price;
      }
    }

    if (!empty($this->customer)) {
      return [$this->customer => $sorted_data[$this->customer] * $selected_currency_rate];
    } else {
      foreach ($sorted_data as $k => $v) {
        $sorted_data[$k] = $v * $selected_currency_rate;
      }
      return $sorted_data;
    }
    /* CONVERT ALL TO EUR THEN TO SELECTED CURRENCY */
  }
}
