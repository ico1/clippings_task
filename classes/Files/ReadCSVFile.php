<?php

namespace Files;

class ReadCSVFile
{
  private $file;
  private $delimeter;

  private $file_data = [];

  function __construct($file)
  {
    $this->file = $file;
    $this->setDelimeter();
    $this->readFile();
  }


  private function detectDelimiter($csvFile)
  {
    $delimiters = [";" => 0, "," => 0, "\t" => 0, "|" => 0];

    $handle = fopen($csvFile, "r");
    $firstLine = fgets($handle);
    fclose($handle);
    foreach ($delimiters as $delimiter => &$count) {
      $count = count(str_getcsv($firstLine, $delimiter));
    }

    return array_search(max($delimiters), $delimiters);
  }

  private function setDelimeter()
  {
    $this->delimeter = $this->detectDelimiter($this->file);
  }

  private function readFile()
  {


    $file = fopen($this->file, 'r');
    $row = 0;
    $col_titles = [];
    while (($line = fgetcsv($file, 1000, $this->delimeter)) !== FALSE) {

      $current_row = [];
      for ($i = 0; $i < count($line); $i++) {
        if ($row == 0) {
          $col_titles[] = $line[$i];
        } else {
          $title = $col_titles[$i];
          $value = $line[$i];
          $current_row[$title] = $value;
        }
      }
      if ($row > 0) {
        $this->file_data[] = $current_row;
      }


      $row++;
    }
    fclose($file);
  }

  public function getFileData()
  {
    return $this->file_data;
  }
}
