<?php

require "includes/autoloader.php";


use Calculations as Calcs;


if (isset($_POST) && isset($_POST['getSum']) && isset($_FILES)) {
  $selected_customer = $_POST['customers'];
  $selected_currency = $_POST['currency'];


  $uploaded_file = $_FILES['csv_file']['tmp_name'];

  $file = new Files\ReadCSVFile($uploaded_file);

  $file_data = $file->getFileData();


  $currencies = ['EUR' => 1, 'USD' => 0.987, 'GBP' => 0.878];

  $calculate_total_price = new Calcs\CalculatePrice();

  $calculate_total_price->setData($file_data);
  $calculate_total_price->setCurrencies($currencies);
  $calculate_total_price->setCurrency($selected_currency);
  $calculate_total_price->setCustomer($selected_customer);

  $total_price = $calculate_total_price->getTotal();

  echo json_encode($total_price);
}
